# get service path, please use -p | -path | --path 
while [ $# -gt 0 ]; do
  case "$1" in
    -p|-path|--path)
      path="$2"
      ;;
    *)
      printf "***************************\n"
      printf "* Error: Invalid argument.*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
  shift
done

cd ./$path
protoc *.proto --go_out=plugins=grpc:. --go_opt=paths=source_relative